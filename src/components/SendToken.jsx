import React from 'react';
import { useWeb3Transfer, useMoralis } from "react-moralis";

    const SendToken = () => {
        const { user, Moralis } = useMoralis();
        const { fetch, error, ErrorMessage, isFetching } = useWeb3Transfer({
            amount: Moralis.Units.Token(0.5, 18),
            receiver: "0x81044440c496373D1E79E5EE7F9445CF9cE367DD",
            type: "erc20",
            contractAddress: "0xdb8d83dc28a108a1572f1d1e755c64612cc16e7a",
        });
        
    return (
    // Use your custom error component to show errors
    <div>
        {error && <ErrorMessage error={error} />}
        <button onClick={() => fetch()} disabled={isFetching}>
        Transfer
        </button>
    </div>
    );
};

export default SendToken