import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { MoralisProvider } from 'react-moralis';

ReactDOM.render(
  <React.StrictMode>
    <MoralisProvider serverUrl="https://xncoy4p8hzng.usemoralis.com:2053/server" appId="c5chTDczjNn9u58Sw7ffH78PcuZTSv4RZCudBBj3">
    <App />
    </MoralisProvider>
  </React.StrictMode>,
  document.getElementById('root')
);






